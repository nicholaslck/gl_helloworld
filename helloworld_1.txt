/*
 * main.cpp
 *
 *  Created on: Mar 2, 2017
 *      Author: nicholas
 */
#include <iostream>
#include <GL/glew.h>
#include <GL/freeglut.h>


using namespace std;

bool* keyStates = new bool[256];
bool* keySpecialStates = new bool[246];
bool movingUp = false; // Whether or not we are moving up or down
float yLocation = 0.0f; // Keep track of our position on the y axis.
float yRotationAngle = 0.0f; // The angle of rotation for our object

void keyPressed(unsigned char key, int x, int y){
	if(key == 'q'){
		glClear (GL_COLOR_BUFFER_BIT);
		exit(0);
	}
}

void renderPrimitive(){
	glBegin(GL_QUADS); // Start drawing a line primitive

		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f); // The bottom left corner
		glVertex3f(-1.0f, 1.0f, 0.0f); // The top left corner

		glColor3f(0.0f, 1.0f, 1.0f);
		glVertex3f(1.0f, 1.0f, 0.0f); // The top right corner
		glVertex3f(1.0f, -1.0f, 0.0f); // The bottom right corner

	glEnd();
}

void display2(){
//	keySpecialOperations();
	glEnable(GL_BLEND); // Enable the OpenGL Blending functionality
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glClearColor(1.f, 1.f, 1.f, 1.f);

	glClear (GL_COLOR_BUFFER_BIT);	//Clear the colour buffer (more buffers later on)
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -5.0f); //Camera translation

	glScalef(0.5f, 1.0f, 2.0f);
	//glScalef(2.0f, 1.0f, 0.5f); //reverse of the scale
	glColor4f(0.0f, 1.0f, 0.0f, 1.0f); // Set the colour to green and fully opaqueq
	glutSolidTeapot(2.0f); //Create a 3D teapot
	//renderPrimitive();
	glTranslatef(0.0f, yLocation, 0.0f); //Object translation on y-axis
	glRotatef(yRotationAngle, 0.0f, 1.0f, 0.0f); //Object rotation on y-axis
	glColor4f(0.0f, 1.0f, 0.0f, 0.5f);
	// Set the colour to green and fully opaqueq

	glutSolidCube(2.0f); //Create a 3D teapot
	glutSwapBuffers();
//	glFlush();

	if (movingUp) // If we are moving up
	yLocation -= 0.005f; // Move up along our yLocation
	else  // Otherwise
	yLocation += 0.005f; // Move down along our yLocation

	if (yLocation < -3.0f) // If we have gone up too far
	movingUp = false; // Reverse our direction so we are moving down
	else if (yLocation > 3.0f) // Else if we have gone down too far
	movingUp = true; // Reverse our direction so we are moving up

	yRotationAngle += 0.005f; // Increment our rotation value
	if (yRotationAngle > 360.0f) // If we have rotated beyond 360 degrees (a full rotation)
	yRotationAngle -= 360.0f; // Subtract 360 degrees off of our rotation
}

GLint LoadGLTexture(const char *filename, int width, int height)
{
     GLuint texture;
     unsigned char *data;
     FILE *file;
 
    //CODE SEGMENT 1
     // open texture data
     file = fopen(filename, "C:\\Users\\annette\\My Pictures\\face.png");
     if (file == NULL) return 0;
 
     // allocate buffer
     data = (unsigned char*) malloc(width * height * 4);
 
     //read texture data
     fread(data, width * height * 4, 1, file);
     fclose(file);
     
    
    //CODE SEGMENT 2
    texture = SOIL_load_OGL_texture // load an image file directly as a new OpenGL texture 
	(
		"C:\\Users\\annette\\My Pictures\\face.png",
		SOIL_LOAD_AUTO,
		SOIL_CREATE_NEW_ID,
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
	);
	
	
    // check for an error during the load process 
    if(texture == 0)
    {
	    //printf( "SOIL loading error: '%s'\n", SOIL_last_result() );
    }
      
    glGenTextures(1, &texture); // allocate a texture name
    glBindTexture(GL_TEXTURE_2D, texture); // select our current texture
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);  
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_DECAL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_DECAL);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);  // when texture area is small, bilinear filter the closest mipmap
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // when texture area is large, bilinear filter the first mipmap
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);  // texture should tile
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    gluBuild2DMipmaps(GL_TEXTURE_2D, 4, width, height, GL_RGBA, GL_UNSIGNED_BYTE, data); // build our texture mipmaps
    free(data);  // free buffer
 
    return texture;
}
void display(){
    glClear(GL_COLOR_BUFFER_BIT);
    glPushMatrix();
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glRotatef(1.0f, 0.0f, 0.0f, 0.0f);
    GLuint texture = LoadGLTexture("face.png", 396, 734);
    glEnable(GL_TEXTURE_2D);
    glBindTexture( GL_TEXTURE_2D, texture );
}

void reshape(int width, int height){
	glViewport(0,0,(GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
    gluPerspective(60, (GLfloat)width / (GLfloat)height, 1.0, 100.0); // Set the Field of view angle (in degrees), the aspect ratio of our window, and the new and far planes

    glMatrixMode(GL_MODELVIEW);
}

int main(int argc, char **argv){
//	cout << "started"<< endl;
	glutInit(&argc, argv);
//	glutInitDisplayMode(GLUT_SINGLE);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowSize (500,500);
	glutInitWindowPosition(100,100);
	glutCreateWindow("Hello World");

	glutDisplayFunc(display);

	glutIdleFunc(display);

	glutReshapeFunc(reshape);

	glutKeyboardFunc(keyPressed);
//	glutKeyboardUpFunc(keyUp);
	glutMainLoop();

	return 1;
}

