#include <iostream>
#include <vector>
#include <GL/glew.h>
#include <GL/freeglut.h>
//#include <GL/glut.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgcodecs/imgcodecs.hpp>
#include <opencv2/videoio/videoio.hpp>
//#include <opencv2/imgproc/imgproc.hpp>
#define WindowWidth 400
#define WindowHeight 400


using namespace std;
using namespace cv;


//static GLuint textureTrash;
vector<GLuint> tex(2);
VideoCapture cap1;
VideoCapture cap2;

GLuint matToTexture(cv::Mat &mat, GLenum minFilter, GLenum magFilter, GLenum wrapFilter, GLuint tex_num)
{
	// Generate a number for our textureID's unique handle
	GLuint textureID;
	glGenTextures(tex_num, &textureID);

	// Bind to our texture handle
	glBindTexture(GL_TEXTURE_2D, textureID);

	// Catch silly-mistake texture interpolation method for magnification
	if (magFilter == GL_LINEAR_MIPMAP_LINEAR  ||
	    magFilter == GL_LINEAR_MIPMAP_NEAREST ||
	    magFilter == GL_NEAREST_MIPMAP_LINEAR ||
	    magFilter == GL_NEAREST_MIPMAP_NEAREST)
	{
		cout << "You can't use MIPMAPs for magnification - setting filter to GL_LINEAR" << endl;
		magFilter = GL_LINEAR;
	}

	// Set texture interpolation methods for minification and magnification
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magFilter);

	// Set texture clamping method
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapFilter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapFilter);

	// Set incoming texture format to:
	// GL_BGR       for CV_CAP_OPENNI_BGR_IMAGE,
	// GL_LUMINANCE for CV_CAP_OPENNI_DISPARITY_MAP,
	// Work out other mappings as required ( there's a list in comments in main() )
	GLenum inputColourFormat = GL_BGR;
	if (mat.channels() == 1)
	{
		inputColourFormat = GL_LUMINANCE;
	}

	// Create the texture
	glTexImage2D(GL_TEXTURE_2D,     // Type of texture
	             0,                 // Pyramid level (for mip-mapping) - 0 is the top level
	             GL_RGB,            // Internal colour format to convert to
	             mat.cols,          // Image width  i.e. 640 for Kinect in standard mode
	             mat.rows,          // Image height i.e. 480 for Kinect in standard mode
	             0,                 // Border width in pixels (can either be 1 or 0)
	             inputColourFormat, // Input image format (i.e. GL_RGB, GL_RGBA, GL_BGR etc.)
	             GL_UNSIGNED_BYTE,  // Image data type
	             mat.ptr());        // The actual image data itself

	// If we're using mipmaps then generate them. Note: This requires OpenGL 3.0 or higher
//	if (minFilter == GL_LINEAR_MIPMAP_LINEAR  ||
//	    minFilter == GL_LINEAR_MIPMAP_NEAREST ||
//	    minFilter == GL_NEAREST_MIPMAP_LINEAR ||
//	    minFilter == GL_NEAREST_MIPMAP_NEAREST)
//	{
//		glGenerateMipmap(GL_TEXTURE_2D);
//	}

	return textureID;
}

void cv_loadImage(VideoCapture cap, GLuint tex_num){
	Mat image;
	cap >> image;
	if(image.empty()){
		      cout << "image empty" << endl;
		  }else{
//			  cout <<"In" << endl; //debug usage
		      tex[tex_num-1] = matToTexture(image, GL_NEAREST, GL_NEAREST, GL_CLAMP, tex_num);
//		      glBindTexture(GL_TEXTURE_2D, tex);
		  }
}
int i;
void gl_display(){
//	cout << tex[0] << endl;


//	Mat image = imread("s1.jpg",CV_LOAD_IMAGE_COLOR);
//		if(image.empty()){
//			      cout << "image empty" << endl;
//			  }else{
//				  cout <<"In" << endl;
//			      tex = matToTexture(image, GL_NEAREST, GL_NEAREST, GL_CLAMP);
//			      glBindTexture(GL_TEXTURE_2D, tex);
//			  }
	glEnable (GL_TEXTURE_2D);
	cv_loadImage(cap1, 1);
	// Right
//	glClearColor(1.f, 1.f, 1.f, 1.f);
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	//Clear the colour buffer (more buffers later on)
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -2.0f); //Camera translation
	//glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
//	glRotatef(180.0f, 0.0f, 0.0f, 1.0f);

	glBindTexture(GL_TEXTURE_2D, tex[0]);
	glBegin(GL_QUADS);
//		glNormal3d(1, 0, 0);
	glTexCoord2f(1, 1);
	glVertex3f(-1.0f, -1.0f, 0.0f); //bottom left
	glTexCoord2f(1, 0);
	glVertex3f(-1.0f, 1.0f, 0.0f); //top left
	glTexCoord2f(0, 0);
	glVertex3f(1.0f, 1.0f, 0.0f); //top right
	glTexCoord2f(0, 1);
	glVertex3f(1.0f, -1.0f, 0.0f); //bottom right
	glEnd();
//	cout << tex[0] << endl;
	cv_loadImage(cap2, 2);
	glBindTexture(GL_TEXTURE_2D, tex[1]);
	glBegin(GL_QUADS);
	//		glNormal3d(1, 0, 0);
	glTexCoord2f(1, 1);
	glVertex3f(1.1f, -1.0f, 0.0f); //bottom left
	glTexCoord2f(1, 0);
	glVertex3f(1.1f, 1.0f, 0.0f); //top left
	glTexCoord2f(0, 0);
	glVertex3f(3.1f, 1.0f, 0.0f); //top right
	glTexCoord2f(0, 1);
	glVertex3f(3.1f, -1.0f, 0.0f); //bottom right
	glEnd();
//	cout << tex[1] << flush;

	unsigned char* buffer = new unsigned char[WindowWidth*WindowHeight*3];
	glReadPixels(0, 0, WindowWidth, WindowHeight, GL_BGR, GL_UNSIGNED_BYTE, buffer);
	Mat image3(WindowWidth, WindowHeight, CV_8UC3, buffer);
	//cv::imwrite("result_1.jpg", image3);

	//MARK: save images

	glutSwapBuffers();

	glDeleteTextures(1, &tex[0]);
	glDeleteTextures(1, &tex[1]);
//	i++;
//	if(i > 50){
//		exit(0);
//	}

}

void gl_reshape(int width, int height){
	glViewport(0,0,(GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
    gluPerspective(60, (GLfloat)width / (GLfloat)height, 1.0, 100.0); // Set the Field of view angle (in degrees), the aspect ratio of our window, and the new and far planes

    glMatrixMode(GL_MODELVIEW);
}

int main(int argc, char **argv) {
	cap1.open(argv[1]);
	cap2.open(argv[2]);
	if(!cap1.isOpened() || !cap2.isOpened()){
		cout << "failed to open video file" << endl;
		return -1;
	}
	i = 0;

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE);
	glutInitWindowSize (WindowWidth,WindowHeight);
    glutInitWindowPosition(100,100);
    glutCreateWindow("Hello World");

//    glutHideWindow();
	glutDisplayFunc(gl_display);
	glutIdleFunc(gl_display);
	glutReshapeFunc(gl_reshape);

	glutMainLoop();

	return 0;
}
